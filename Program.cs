﻿using GetSlashdotStories.Services;
using GetSlashdotStories.Services.Interfaces;
using GetSlashdotStories.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.IO;

namespace GetSlashdotStories
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var dataImportService = serviceProvider.GetService<IDataImportService>();
                dataImportService.ImportStoriesFromUrl();
            }            
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDataImportService, DataImportService>();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            services.AddOptions();
            services.Configure<Settings>(configuration.GetSection("Settings"));
        }
    }
}
