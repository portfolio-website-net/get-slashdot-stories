﻿namespace GetSlashdotStories.Services.Interfaces
{
    public interface IDataImportService
    {
        void ImportStoriesFromUrl();        
    }
}
