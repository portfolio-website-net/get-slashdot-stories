﻿using System;
using System.Linq;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Network;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using GetSlashdotStories.Services.Interfaces;
using GetSlashdotStories.Models;

namespace GetSlashdotStories.Services
{
    public class DataImportService : IDataImportService
    {
        private Stopwatch _stopwatch = new Stopwatch();

        private readonly Settings _settings;

        public DataImportService(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        public void ImportStoriesFromUrl()
        {
            _stopwatch.Start();

            var browser = new ScrapingBrowser();
            var isValidPage = false;
            var pageNum = 1;

            var context = new Context(_settings.DbConnectionString);

            do
            {
                isValidPage = false;
                
                var page = browser.NavigateToPage(new Uri(_settings.BaseDataUrl + pageNum.ToString()));
                var stories = page.Html.CssSelect("article");                

                foreach (var story in stories)
                {
                    var links = story.CssSelect("span.story-title a");
                    var byLine = story.CssSelect("span.story-byline");
                    var time = story.CssSelect("time");
                    var topic = story.CssSelect("span.topic a img");
                    var topicText = string.Empty;
                    var title = string.Empty;
                    var discussionLink = string.Empty;                    
                    var sourceLink = string.Empty;       
                    var submitter = string.Empty;       
                    var dateTime = new DateTime(); 
                    var department = story.CssSelect(".dept-text");      
                    var departmentText = string.Empty;
                    var description = story.CssSelect(".body");      
                    var descriptionText = string.Empty;

                    if (time.Any())
                    {
                        if (topic.Any())
                        {
                            if (topic.First().Attributes["title"] != null)
                            {
                                topicText = topic.First().Attributes["title"].Value;
                            }
                        }

                        foreach (var link in links)
                        {
                            if (link.Attributes["class"] != null
                                && link.Attributes["class"].Value.Contains("story-sourcelnk"))
                            {
                                sourceLink = link.Attributes["href"].Value;
                            }
                            else
                            {
                                title = link.InnerText;
                                discussionLink = link.Attributes["href"].Value;                            
                            }
                        }

                        if (byLine.Any())
                        {
                            var byLineText = RemoveExtraWhiteSpace(byLine.First().InnerText).Trim();
                            if (byLineText.Contains("Posted by"))
                            {
                                var searchText = "Posted by";
                                var startPos = byLineText.IndexOf(searchText) + searchText.Length;
                                var endPos = byLineText.IndexOf(" on ", startPos);
                                submitter = RemoveExtraWhiteSpace(byLineText.Substring(startPos, endPos - startPos)).Trim();
                            }
                        }     

                        if (department.Any())
                        {
                            departmentText = RemoveExtraWhiteSpace(department.First().InnerText).Trim();                            
                        }   

                        if (description.Any())
                        {
                            descriptionText = RemoveExtraWhiteSpace(description.First().InnerText).Trim();                            
                        }               
                    
                        DateTime.TryParse(time.First().InnerText
                            .Replace("on ", string.Empty)
                            .Replace("@", string.Empty), out dateTime);
                        
                        if (!string.IsNullOrEmpty(title))
                        {
                            context.Add(
                                new Story {
                                    Topic = topicText,
                                    Title = title,
                                    DiscussionLink = discussionLink,
                                    SourceLink = sourceLink,
                                    Submitter = submitter,
                                    Date = dateTime,
                                    Department = departmentText,
                                    Description = descriptionText
                                }
                            );                       

                            isValidPage = true;
                        }
                    }
                }

                if (isValidPage)
                {
                    Console.WriteLine($"Page #: {pageNum}");
                }

                if (pageNum % 10 == 0)
                {
                    context.SaveChanges();
                    context = new Context(_settings.DbConnectionString);                    
                }

                pageNum++;
            }
            while (isValidPage);

            context.SaveChanges();

            _stopwatch.Stop();
            Console.WriteLine("Total time elapsed: {0:hh\\:mm\\:ss}", _stopwatch.Elapsed);
        }

        private string RemoveExtraWhiteSpace(string text)
        {
            return Regex.Replace(text, @"\s+", " ");
        }
    }
}
