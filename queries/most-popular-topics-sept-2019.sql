SELECT TOP 10 [Topic],
       COUNT(1) AS [Number of Stories]
  FROM [Stories]
 WHERE [Date] BETWEEN '2019-09-01' AND '2019-9-30'
 GROUP BY [Topic]
 ORDER BY COUNT(1) DESC