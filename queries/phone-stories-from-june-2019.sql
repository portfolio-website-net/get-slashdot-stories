SELECT [Title],      
	   [Date],
       [Department]
  FROM [Stories]
 WHERE [Title] LIKE '%phone%'
   AND [Date] BETWEEN '2019-06-01' AND '2019-06-30'
 ORDER BY [Date]
