SELECT [Department],
       COUNT(1) AS [Number of Stories]
  FROM [Stories]
 WHERE [Date] BETWEEN '2019-01-01' AND '2019-06-30'
 GROUP BY [Department]
HAVING COUNT(1) > 50
 ORDER BY COUNT(1) DESC
