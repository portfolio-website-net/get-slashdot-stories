IF OBJECT_ID('dbo.[Stories]', 'U') IS NOT NULL 
BEGIN 
	DROP TABLE dbo.[Stories] 
END

CREATE TABLE [dbo].[Stories](
	[StoryId] [int] IDENTITY(1,1) NOT NULL,
	[Topic] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NOT NULL,
	[DiscussionLink] [nvarchar](max) NULL,
	[SourceLink] [nvarchar](max) NULL,
	[Submitter] [nvarchar](max) NULL,
	[Date] [date] NULL,
	[Department] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	CONSTRAINT [PK_Stories] PRIMARY KEY CLUSTERED 
	(
		[StoryId] ASC
	)
) 
