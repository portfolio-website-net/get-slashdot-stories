using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
 
namespace GetSlashdotStories.Models
{
    public class Story
    { 
        [Key]
        public int StoryId { get; set; }

        public string Topic { get; set; }

        public string Title { get; set; }

        public string DiscussionLink { get; set; }

        public string SourceLink { get; set; }        

        public string Submitter { get; set; }

        public DateTime Date { get; set; }

        public string Department { get; set; }

        public string Description { get; set; }
    }
}