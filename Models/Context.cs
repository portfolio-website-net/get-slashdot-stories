using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Options;
 
namespace GetSlashdotStories.Models
{
    public class Context : DbContext
    {
        private readonly string _dbConnectionString;

        public Context(string dbConnectionString)
        {
            _dbConnectionString = dbConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(_dbConnectionString);
        }
 
        public DbSet<Story> Stories { get; set; }
    }
}