﻿namespace GetSlashdotStories
{
    public class Settings
    {
        public string DbConnectionString { get; set; }

        public string BaseDataUrl { get; set; }
    }
}
